package dev.balakumar.orderapi.exception;

public class ExternalApiException extends RuntimeException {
    public ExternalApiException(String s) {
        super(s);
    }
}
