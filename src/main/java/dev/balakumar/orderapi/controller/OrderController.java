package dev.balakumar.orderapi.controller;

import dev.balakumar.orderapi.dto.OrderConfirmation;
import dev.balakumar.orderapi.dto.OrderRequest;
import dev.balakumar.orderapi.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }


    @Operation(summary = "Submits an order to the backend for processing", description = "Returns a orderId and an order status")
    @PostMapping("orders")
    public ResponseEntity<OrderConfirmation> submitOrder(@Valid @RequestBody OrderRequest orderRequest) {
        log.debug("Received request: {}", orderRequest);
        OrderConfirmation orderConfirmation = orderService.submitOrder(orderRequest);
        log.debug("Returning response: {}", orderConfirmation);
        return new ResponseEntity<>(orderConfirmation, HttpStatus.CREATED);
    }
}
