package dev.balakumar.orderapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.HttpUrlConnectionMessageSender;

@Configuration
public class SoapClientConfig {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("dev.balakumar.ordersystem");
        return marshaller;
    }

    @Bean
    public WebServiceTemplate orderSystemClient(Jaxb2Marshaller marshaller) {
        WebServiceTemplate client = new WebServiceTemplate();
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        client.setMessageSender(new HttpUrlConnectionMessageSender());
        return client;
    }
}

