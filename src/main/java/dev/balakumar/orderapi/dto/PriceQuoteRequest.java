package dev.balakumar.orderapi.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceQuoteRequest {
    String productId;
    BillingAddress billingAddress;
}
