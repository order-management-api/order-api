package dev.balakumar.orderapi.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BillingAddress {

    @NotBlank
    String addressLine;

    @NotBlank
    String countryCode;

    @NotBlank
    String pinCode;

    @Email
    String email;
}
