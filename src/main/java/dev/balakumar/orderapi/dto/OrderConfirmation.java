package dev.balakumar.orderapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrderConfirmation {
    String orderId;
    String orderStatus;
    BigDecimal totalPrice;

    String email;
}
