package dev.balakumar.orderapi.service;

import dev.balakumar.orderapi.dto.OrderConfirmation;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailService {

    private final RabbitTemplate rabbitTemplate;

    @Value("${email.queue}")
    private String emailQueue;

    final AmqpAdmin amqpAdmin;


    public EmailService(RabbitTemplate rabbitTemplate, AmqpAdmin amqpAdmin) {
        this.rabbitTemplate = rabbitTemplate;

        this.amqpAdmin = amqpAdmin;
    }

    @PostConstruct
    void init() {
        if (amqpAdmin.getQueueInfo(emailQueue) == null) {
            log.info("{} did not exist, creating one.", emailQueue);
            amqpAdmin.declareQueue(new Queue(emailQueue));
        }
    }

    @Async
    public void sendOrderConfirmation(OrderConfirmation orderConfirmation, String email) {
        orderConfirmation.setEmail(email);
        rabbitTemplate.convertAndSend(emailQueue, email);
    }
}

