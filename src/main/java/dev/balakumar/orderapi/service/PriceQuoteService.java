package dev.balakumar.orderapi.service;

import dev.balakumar.orderapi.dto.BillingAddress;
import dev.balakumar.orderapi.dto.OrderRequest;
import dev.balakumar.orderapi.dto.PriceQuote;
import dev.balakumar.orderapi.dto.PriceQuoteRequest;
import dev.balakumar.orderapi.exception.ExternalApiException;
import dev.balakumar.orderapi.exception.ProductNotFoundException;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryRegistry;
import io.vavr.control.Try;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PriceQuoteService {

    private final RestTemplate restTemplate;
    private final CircuitBreakerRegistry circuitBreakerRegistry;
    private final RetryRegistry retryRegistry;

    @Value("${price-quote-api.url}")
    String priceQuoteApiUrl;

    public PriceQuoteService(RestTemplate restTemplate, CircuitBreakerRegistry circuitBreakerRegistry, RetryRegistry retryRegistry) {
        this.restTemplate = restTemplate;
        this.circuitBreakerRegistry = circuitBreakerRegistry;
        this.retryRegistry = retryRegistry;
    }

    public PriceQuote getPriceQuote(OrderRequest orderRequest) {
        String productId = orderRequest.getProductId();
        BillingAddress billingAddress = orderRequest.getBillingAddress();

        PriceQuoteRequest request = new PriceQuoteRequest();
        request.setProductId(productId);
        request.setBillingAddress(billingAddress);

        CircuitBreaker circuitBreaker = circuitBreakerRegistry.circuitBreaker("priceQuoteService");
        Retry retry = retryRegistry.retry("priceQuoteService");

        return Try.ofSupplier(circuitBreaker.decorateSupplier(() -> retry.executeSupplier(() ->
                restTemplate.postForObject(priceQuoteApiUrl, request, PriceQuote.class)
        ))).recover(throwable -> {
            // Handle exceptions and provide fallback if necessary
            if (throwable instanceof ProductNotFoundException) {
                throw (ProductNotFoundException) throwable;
            } else {
                throw new ExternalApiException("Error calling Product Catalog API: " + throwable.getMessage());
            }
        }).getOrElseThrow(() -> new RuntimeException("Failed to get price quote from Price Quote Service"));
    }
}
