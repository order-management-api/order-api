package dev.balakumar.orderapi.service;

import dev.balakumar.orderapi.dto.OrderConfirmation;
import dev.balakumar.orderapi.dto.OrderRequest;
import dev.balakumar.orderapi.dto.PriceQuote;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.RetryRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
@Slf4j
@Service
public class OrderService {

    private final PriceQuoteService priceQuoteService;

    private final OrderSystemService orderSystemService;

    private final EmailService emailService;

    private final CircuitBreakerRegistry circuitBreakerRegistry;

    private final RetryRegistry retryRegistry;

    public OrderService(PriceQuoteService priceQuoteService, OrderSystemService orderSystemService, EmailService emailService, CircuitBreakerRegistry circuitBreakerRegistry, RetryRegistry retryRegistry) {
        this.priceQuoteService = priceQuoteService;
        this.orderSystemService = orderSystemService;
        this.emailService = emailService;
        this.circuitBreakerRegistry = circuitBreakerRegistry;
        this.retryRegistry = retryRegistry;
    }

    public OrderConfirmation submitOrder(OrderRequest orderRequest) {

        // Get the price quote
        PriceQuote priceQuote = priceQuoteService.getPriceQuote(orderRequest);

        // Submit the order to the Order System
        OrderConfirmation orderConfirmation = orderSystemService.submitOrder(orderRequest, priceQuote.getTotalPrice());

        // Send a confirmation email
        try {
            emailService.sendOrderConfirmation(orderConfirmation, orderRequest.getBillingAddress().getEmail());
        } catch(Exception e) {
            log.error("Error sending the email message to the queue: {}", e.getMessage());
        }

        return orderConfirmation;
    }
}
