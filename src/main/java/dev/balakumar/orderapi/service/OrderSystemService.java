package dev.balakumar.orderapi.service;

import dev.balakumar.orderapi.dto.BillingAddress;
import dev.balakumar.orderapi.dto.OrderConfirmation;
import dev.balakumar.orderapi.dto.OrderRequest;
import dev.balakumar.ordersystem.ProcessOrderRequest;
import dev.balakumar.ordersystem.ProcessOrderResponse;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryRegistry;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import java.math.BigDecimal;

@Slf4j
@Service
public class OrderSystemService {

    private final CircuitBreakerRegistry circuitBreakerRegistry;
    private final RetryRegistry retryRegistry;
    private final WebServiceTemplate webServiceTemplate;

    @Value("${order-system.url:http://localhost:8089/ordersystem}")
    private String ORDER_SYSTEM_ENDPOINT;

    public OrderSystemService(CircuitBreakerRegistry circuitBreakerRegistry, RetryRegistry retryRegistry, WebServiceTemplate webServiceTemplate) {
        this.circuitBreakerRegistry = circuitBreakerRegistry;
        this.retryRegistry = retryRegistry;
        this.webServiceTemplate = webServiceTemplate;
    }

    public OrderConfirmation submitOrder(OrderRequest orderRequest, BigDecimal totalPrice) {
        try {
            int productId = Integer.parseInt(orderRequest.getProductId());
            int quantity = orderRequest.getQuantity();
            BillingAddress billingAddress = orderRequest.getBillingAddress();

            ProcessOrderRequest processOrderRequest = new ProcessOrderRequest();
            processOrderRequest.setProductId(productId);
            processOrderRequest.setQuantity(quantity);
            processOrderRequest.setBillingAddress(billingAddress.toString());
            processOrderRequest.setPaymentAmount(totalPrice.doubleValue());

            CircuitBreaker circuitBreaker = circuitBreakerRegistry.circuitBreaker("orderSystemService");
            Retry retry = retryRegistry.retry("orderSystemService");

            ProcessOrderResponse soapResponse = Try.ofSupplier(circuitBreaker.decorateSupplier(() -> retry.executeSupplier(() ->
                            (ProcessOrderResponse) webServiceTemplate.marshalSendAndReceive(ORDER_SYSTEM_ENDPOINT, processOrderRequest))))
                    .getOrElseThrow(() -> new RuntimeException("Failed to submit order to Order System"));

            return new OrderConfirmation(soapResponse.getOrderId(), soapResponse.getStatus(), totalPrice, orderRequest.getBillingAddress().getEmail());
        } catch (Exception e) {
            throw new RuntimeException("Error calling OrderSystem SOAP service", e);
        }
    }
}
