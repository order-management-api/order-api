package dev.balakumar.orderapi;

import dev.balakumar.orderapi.service.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class OrderApiApplicationTests {
    @MockBean
    EmailService emailService;

    @Test
    void contextLoads() {
    }

}
