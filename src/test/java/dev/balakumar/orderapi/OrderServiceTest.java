package dev.balakumar.orderapi;

import dev.balakumar.orderapi.dto.BillingAddress;
import dev.balakumar.orderapi.dto.OrderConfirmation;
import dev.balakumar.orderapi.dto.OrderRequest;
import dev.balakumar.orderapi.dto.PriceQuote;
import dev.balakumar.orderapi.service.EmailService;
import dev.balakumar.orderapi.service.OrderService;
import dev.balakumar.orderapi.service.OrderSystemService;
import dev.balakumar.orderapi.service.PriceQuoteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class OrderServiceTest {

    @Mock
    private PriceQuoteService priceQuoteService;

    @Mock
    private OrderSystemService orderSystemService;

    @Mock
    private EmailService emailService;

    @InjectMocks
    private OrderService orderService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test submitOrder success")
    void submitOrder_success() {
        // Prepare input and mocked objects
        OrderRequest orderRequest = new OrderRequest("1234", 1, new BillingAddress("123 Street, City", "US", "12", "test@email.com"));
        PriceQuote priceQuote = new PriceQuote(BigDecimal.valueOf(100.00));
        OrderConfirmation orderConfirmation = new OrderConfirmation("1234", "SUCCESS", BigDecimal.valueOf(100.00), "test@email.com");

        // Set up the expected behavior for mocked services
        when(priceQuoteService.getPriceQuote(orderRequest)).thenReturn(priceQuote);
        when(orderSystemService.submitOrder(orderRequest, priceQuote.getTotalPrice())).thenReturn(orderConfirmation);

        // Call the method to be tested
        OrderConfirmation result = orderService.submitOrder(orderRequest);

        // Assert the result and verify interactions
        assertEquals(orderConfirmation, result);
        verify(emailService, times(1)).sendOrderConfirmation(orderConfirmation, "test@email.com");
    }

    @Test
    @DisplayName("Test submitOrder with email failure")
    void submitOrder_emailFailure() {
        // Prepare input and mocked objects
        OrderRequest orderRequest = new OrderRequest("1234", 1, new BillingAddress("123 Street, City", "US", "12", "test@email.com"));
        PriceQuote priceQuote = new PriceQuote(BigDecimal.valueOf(100.00));
        OrderConfirmation orderConfirmation = new OrderConfirmation("1234", "SUCCESS", BigDecimal.valueOf(100.00), "test@email.com");

        // Set up the expected behavior for mocked services
        when(priceQuoteService.getPriceQuote(orderRequest)).thenReturn(priceQuote);
        when(orderSystemService.submitOrder(orderRequest, priceQuote.getTotalPrice())).thenReturn(orderConfirmation);
        doThrow(new RuntimeException("Email sending failed")).when(emailService).sendOrderConfirmation(orderConfirmation, "test@email.com");

        // Call the method to be tested
        OrderConfirmation result = orderService.submitOrder(orderRequest);

        // Assert the result and verify interactions
        assertEquals(orderConfirmation, result);
        verify(emailService, times(1)).sendOrderConfirmation(orderConfirmation, "test@email.com");
    }

    @Test
    @DisplayName("Test submitOrder with price quote failure")
    void submitOrder_priceQuoteFailure() {
        // Prepare input and mocked objects
        OrderRequest orderRequest = new OrderRequest("1234", 1, new BillingAddress("123 Street, City", "US", "12", "test@email.com"));

        // Set up the expected behavior for mocked services
        when(priceQuoteService.getPriceQuote(orderRequest)).thenThrow(new RuntimeException("Price quote service failed"));

        // Call the method to be tested and assert the exception
        assertThrows(RuntimeException.class, () -> orderService.submitOrder(orderRequest));

        // Verify interactions
        verify(orderSystemService, never()).submitOrder(any(OrderRequest.class), any(BigDecimal.class));
        verify(emailService, never()).sendOrderConfirmation(any(OrderConfirmation.class), anyString());
    }

    @Test
    @DisplayName("Test submitOrder with order system failure")
    void submitOrder_orderSystemFailure() {
        // Prepare input and mocked objects
        OrderRequest orderRequest = new OrderRequest("1234", 1, new BillingAddress("123 Street, City", "US", "12", "test@email.com"));
        PriceQuote priceQuote = new PriceQuote(BigDecimal.valueOf(100.00));

        // Set up the expected behavior for mocked services
        when(priceQuoteService.getPriceQuote(orderRequest)).thenReturn(priceQuote);
        when(orderSystemService.submitOrder(orderRequest, priceQuote.getTotalPrice())).thenThrow(new RuntimeException("Order system submission failed"));

        // Call the method to be tested and assert the exception
        assertThrows(RuntimeException.class, () -> orderService.submitOrder(orderRequest));

        // Verify interactions
        verify(emailService, never()).sendOrderConfirmation(any(OrderConfirmation.class), anyString());
    }

}
