package dev.balakumar.orderapi;

import dev.balakumar.orderapi.controller.OrderController;
import dev.balakumar.orderapi.dto.BillingAddress;
import dev.balakumar.orderapi.dto.OrderConfirmation;
import dev.balakumar.orderapi.dto.OrderRequest;
import dev.balakumar.orderapi.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    private OrderRequest validOrderRequest;
    private OrderConfirmation validOrderConfirmation;

    @BeforeEach
    void setUp() {
        validOrderRequest = new OrderRequest("1234", 1, new BillingAddress("123 Street, City, Country", "US", "51112", "test@email.com"));
        validOrderConfirmation = new OrderConfirmation("orderId123", "SUCCESS", BigDecimal.valueOf(100.00), "test@email.com");
    }

    @Test
    @DisplayName("Test submitOrder with valid request")
    void submitOrder_validRequest() throws Exception {
        when(orderService.submitOrder(validOrderRequest)).thenReturn(validOrderConfirmation);

        mockMvc.perform(post("/api/v1/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"productId\":\"1234\",\"billingAddress\": {\"addressLine\": \"123 Street, City, Country\", \"countryCode\": \"US\", \"pinCode\": \"51112\"},\"quantity\":1}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.orderId").value("orderId123"))
                .andExpect(jsonPath("$.totalPrice").value(100.00))
                .andExpect(jsonPath("$.orderStatus").value("SUCCESS"));

        verify(orderService).submitOrder(validOrderRequest);
    }

    @Test
    @DisplayName("Test submitOrder with invalid request")
    void submitOrder_invalidRequest() throws Exception {
        mockMvc.perform(post("/api/v1/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"billingAddress\": {\"addressLine\": \"123 Street, City, Country\", \"countryCode\": \"US\", \"pinCode\": \"51112\"},\"quantity\":1}"))
                .andExpect(status().isBadRequest());

        verify(orderService, never()).submitOrder(any(OrderRequest.class));
    }

    @Test
    @DisplayName("Test submitOrder with internal server error")
    void submitOrder_internalServerError() throws Exception {
        when(orderService.submitOrder(validOrderRequest)).thenThrow(new RuntimeException("Internal server error"));

        mockMvc.perform(post("/api/v1/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"productId\":\"1234\",\"billingAddress\": {\"addressLine\": \"123 Street, City, Country\", \"countryCode\": \"US\", \"pinCode\": \"51112\"},\"quantity\":1}"))
                .andExpect(status().isInternalServerError());

        verify(orderService).submitOrder(validOrderRequest);
    }
}
